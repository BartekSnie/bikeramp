import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TripsModule } from './trips/trips.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TripEntity } from './db/entities/Trip.entity';
import { StatsModule } from './stats/stats.module';

@Module({
  imports: [TripsModule, TypeOrmModule.forRoot({
    type: 'postgres',
    host: process.env.POSTGRES_HOST,
    port: process.env.POSTGRES_PORT ? parseInt(process.env.POSTGRES_PORT) : 5432,
    username: process.env.POSTGRES_USER,
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB,
    entities: [TripEntity],
    synchronize: true
  }), StatsModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
