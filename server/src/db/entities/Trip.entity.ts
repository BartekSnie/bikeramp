import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class TripEntity extends BaseEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    startingAddress: string;

    @Column()
    destinationAddress: string;

    @Column({
        type: 'float'
    })
    distance: string;
    
    @Column({
        type: 'int'
    })
    price: number;

    @Column({
        type: 'timestamptz'
    })
    date: Date;

    @Column({
        type: 'bigint'
    })
    created: number;
}
