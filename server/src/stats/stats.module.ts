import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DatetimeService } from 'src/core/datetime/Datetime.service';
import { PostgreSqlStatsRepository } from 'src/core/stats/PostgreSqlStats.repository';
import { StatsService } from 'src/core/stats/Stats.service';
import { TripEntity } from 'src/db/entities/Trip.entity';
import { StatsController } from './stats.controller';

@Module({
  imports: [
    TypeOrmModule.forFeature([TripEntity]),
  ],
  controllers: [StatsController],
  providers: [
    {
      provide: 'StatsRepository',
      useClass: PostgreSqlStatsRepository
    },
    {
      provide: 'StatsService',
      useClass: StatsService
    },
    {
      provide: 'DatetimeService',
      useClass: DatetimeService
    }
  ]
})
export class StatsModule {}
