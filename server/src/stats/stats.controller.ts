import { Controller, Get, HttpException, HttpStatus, Inject } from '@nestjs/common';
import { IStatsService } from 'src/core/stats/IStats.service';
import { MonthlyStats } from 'src/core/stats/MonthlyStats';
import { WeeklyStats } from 'src/core/stats/WeeklyStats';

@Controller('stats')
export class StatsController {
    constructor(
        @Inject('StatsService')
        private readonly statsService: IStatsService
    ) { }

    @Get('/weekly')
    public async getWeeklyStats(): Promise<WeeklyStats> {
        try {
            return await this.statsService.getWeekly();
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }

    @Get('/monthly')
    public async getMonthlyStats(): Promise<MonthlyStats[]> {
        try {
            return await this.statsService.getMonthly();
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }
}
