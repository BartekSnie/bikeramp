import { ValueObject } from "../abstract/ValueObject";

export class Address extends ValueObject<string> {
    private constructor(value: string) {
        super(value);
    }

    public static new(value: string): Address {
        if(value === "") {
            throw new Error("Address can't be empty");
        }
        
        return new Address(value);
    }

    public compare(other: Address): boolean {
        return this._value === other._value;
    }
}
