import { ValueObject } from "../abstract/ValueObject";

export class Price extends ValueObject<number> {
    private constructor(value: number) {
        super(value);
    }

    public static new(value: number): Price {
        if (!Number.isInteger(value)) {
            throw new Error('Price must be an integer');
        }

        return new Price(value);
    }
}
