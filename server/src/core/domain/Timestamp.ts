import { ValueObject } from "../abstract/ValueObject";

export class Timestamp extends ValueObject<number> {
    private constructor(value: number) {
        super(value);
    } 

    public static new(value: number): Timestamp {
        return new Timestamp(value);
    }

    public static current(value: number): Timestamp {
        const now = Date.now();
        
        return new Timestamp(now);
    }
}