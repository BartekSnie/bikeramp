import { MonthlyStats } from "./MonthlyStats";
import { WeeklyStats } from "./WeeklyStats";

export interface Props {
    startingDate: string;
    endingDate: string;
}

export interface IStatsRepository {
    getWeekly(props: Props): Promise<WeeklyStats>;
    getMonthly(props: Props): Promise<MonthlyStats[]>;
}
