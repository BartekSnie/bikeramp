import { MonthlyStats } from "./MonthlyStats";
import { WeeklyStats } from "./WeeklyStats";

export interface IStatsService {
    getWeekly(): Promise<WeeklyStats>;
    getMonthly(): Promise<MonthlyStats[]>;
}
