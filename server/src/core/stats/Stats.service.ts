import { Inject } from "@nestjs/common";
import { IDatetimeService } from "../datetime/IDatetime.service";
import { IStatsRepository } from "./IStats.repository";
import { IStatsService } from "./IStats.service";
import { MonthlyStats } from "./MonthlyStats";
import { WeeklyStats } from "./WeeklyStats";

export class StatsService implements IStatsService {
    constructor(
        @Inject('StatsRepository')
        private readonly statsRepository: IStatsRepository,

        @Inject('DatetimeService')
        private readonly datetimeService: IDatetimeService
    ) { }

    async getWeekly(): Promise<WeeklyStats> {
        const now = new Date();
        const startOfTheWeek = this.datetimeService.getDateWithBeginningOfTheWeekFromDate(now);

        return await this.statsRepository.getWeekly({
            startingDate: startOfTheWeek.toISOString(),
            endingDate: now.toISOString()
        });
    }

    async getMonthly(): Promise<MonthlyStats[]> {
        const now = new Date();
        const startOfTheMonth = this.datetimeService.getDateWithBeginningOfTheMonthFromDate(now);

        return await this.statsRepository.getMonthly({
            startingDate: startOfTheMonth.toISOString(),
            endingDate: now.toISOString()
        });
    }

}
