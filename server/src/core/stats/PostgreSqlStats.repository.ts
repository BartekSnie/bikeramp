import { TripEntity } from "src/db/entities/Trip.entity";
import { getManager, getRepository } from "typeorm";
import { IStatsRepository, Props } from "./IStats.repository";
import { MonthlyStats } from "./MonthlyStats";
import { WeeklyStats } from "./WeeklyStats";

export class PostgreSqlStatsRepository implements IStatsRepository {
    async getWeekly(props: Props): Promise<WeeklyStats> {
        const builder = getRepository(TripEntity).createQueryBuilder('trip');

        builder.select(
            `
                CONCAT(COALESCE(SUM(trip.distance), 0), \'km\') AS total_distance,
                CONCAT(ROUND(COALESCE(SUM(trip.price)::decimal / 100, 0::decimal), 2), \'PLN\') AS total_price
            `).where('trip.date >= :startingDate AND trip.date <= :endingDate', {
                startingDate: props.startingDate,
                endingDate: props.endingDate
            });

        return await builder.getRawOne();
    }

    async getMonthly(props: Props): Promise<MonthlyStats[]> {
        const manager = getManager();

        return manager.query(`
        select sq.day,
	        CONCAT(ROUND(SUM(sq.distance)::numeric, 2), 'km') AS distance,
	        CONCAT(ROUND(AVG(sq.distance)::numeric, 2), 'km') as avg_ride,
	        CONCAT(ROUND(COALESCE(AVG(sq.price)::decimal / 100, 0::decimal), 2), 'PLN') AS avg_price from 
                (SELECT TO_CHAR(trip.date, 'FMMonth, FMDDth') AS "day", TO_CHAR(trip.date, 'YYYY-MM-dd') as date, trip.distance, trip.price FROM trip_entity as trip WHERE trip.date >= $1 AND trip.date <= $2) as sq 
            group by sq.date, sq.day order by sq.date
        `, [props.startingDate, props.endingDate]);
    }

}