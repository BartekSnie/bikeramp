import { IDatetimeService } from "./IDatetime.service";

export class DatetimeService implements IDatetimeService {
    getDateWithBeginningOfTheWeekFromDate(date: Date): Date {
        const dayOfTheWeek = date.getDay();
        const day = date.getDate();

        const startOfTheWeek = new Date(date.getTime());
        startOfTheWeek.setDate(day-dayOfTheWeek + (dayOfTheWeek === 0 ? -6 : 1));
        startOfTheWeek.setHours(0);
        startOfTheWeek.setMinutes(0);
        startOfTheWeek.setSeconds(0);
        startOfTheWeek.setMilliseconds(0);

        return startOfTheWeek;
    }
    getDateWithBeginningOfTheMonthFromDate(date: Date): Date {
        const startOfTheMonth = new Date(date.getTime());
        startOfTheMonth.setDate(1);
        startOfTheMonth.setHours(0);
        startOfTheMonth.setMinutes(0);
        startOfTheMonth.setSeconds(0);
        startOfTheMonth.setMilliseconds(0);

        return startOfTheMonth;
    }
    
}