export interface IDatetimeService {
    getDateWithBeginningOfTheWeekFromDate(date: Date): Date;
    getDateWithBeginningOfTheMonthFromDate(date: Date): Date;
}
