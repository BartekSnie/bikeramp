import { Address } from "../domain/Address";
import { Price } from "../domain/Price";

export interface RegisterNewProps {
    startAddress: Address;
    destinationAddress: Address;
    distance: string;
    price: Price;
    date: Date;
    creationDate: number;
}

export interface ITripRepository {
    registerNew(props: RegisterNewProps): Promise<void>;
}
