import { Inject } from "@nestjs/common";
import { TripEntity } from "src/db/entities/Trip.entity";
import { IDistanceService } from "../distance/IDistance.service";
import { Address } from "../domain/Address";
import { IGeocodingService } from "../geocoding/IGeocoding.service";
import { ITripRepository } from "./ITrip.repository";
import { ITripService, RegisterNewProps } from "./ITrip.service";

export class TripService implements ITripService {
    constructor(
        @Inject('DistanceService')
        private readonly distanceService: IDistanceService,

        @Inject('GeocodingService')
        private readonly geocodingService: IGeocodingService,

        @Inject('TripRepository')
        private readonly tripRepository: ITripRepository
    ) { }

    private async getDistance(startingAddress: Address, destinationAddress: Address): Promise<number> {
        if (startingAddress.compare(destinationAddress)) {
            return 0;
        }

        const startingAddressGeocodeResult = await this.geocodingService.geocode(startingAddress);
        const destinationAddressGeocodeResult = await this.geocodingService.geocode(destinationAddress);

        const geocodedStartingAddress = startingAddressGeocodeResult.getFirstResult();
        const geocodedDestinationAddress = destinationAddressGeocodeResult.getFirstResult();

        if (geocodedStartingAddress === null) {
            throw new Error('Couldn\'t geocode starting address');
        }

        if (geocodedDestinationAddress === null) {
            throw new Error('Couldn\'t geocode destintaion address');
        }

        return await this.distanceService.calculate(geocodedStartingAddress.latitude, geocodedStartingAddress.longitude, geocodedDestinationAddress.latitude, geocodedDestinationAddress.longitude);
    }

    async registerNew(props: RegisterNewProps): Promise<void> {
        const now = new Date();
        const tripDate = new Date(props.date);

        if (now < tripDate) {
            throw new Error('Date cannot be higher than actual date.');
        }

        const distance = await this.getDistance(props.startingAddress, props.destinationAddress);

        await this.tripRepository.registerNew({
            startAddress: props.startingAddress,
            destinationAddress: props.destinationAddress,
            creationDate: now.getTime(),
            date: tripDate,
            distance: distance.toFixed(2),
            price: props.price
        });
    }
}
