import { Address } from "../domain/Address";
import { Price } from "../domain/Price";

export interface RegisterNewProps {
    startingAddress: Address;
    destinationAddress: Address;
    price: Price;
    date: string;
}

export interface ITripService {
    registerNew(props: RegisterNewProps): Promise<void>;
}
