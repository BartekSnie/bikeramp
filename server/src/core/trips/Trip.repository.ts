import { TripEntity } from "src/db/entities/Trip.entity";
import { ITripRepository, RegisterNewProps } from "./ITrip.repository";

export class TripRepository implements ITripRepository {
    async registerNew(props: RegisterNewProps): Promise<void> {
        const newTrip = new TripEntity();
        newTrip.created = props.creationDate;
        newTrip.date = props.date;
        newTrip.startingAddress = props.startAddress.value;
        newTrip.destinationAddress = props.destinationAddress.value;
        newTrip.distance = props.distance;
        newTrip.price = props.price.value;

        await newTrip.save();
    }
}
