import { ValueObject } from "../abstract/ValueObject";

export class Latitude extends ValueObject<number> {
    private constructor(value: number) {
        super(value);
    }

    public static new(value: number): Latitude {
        return new Latitude(value);
    }

    public static fromString(value: string): Latitude {
        const parsed = Number.parseFloat(value);

        if (isNaN(parsed)) {
            throw new Error('Not a number');
        }

        return new Latitude(parsed);
    }
}