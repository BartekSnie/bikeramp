import { ValueObject } from "../abstract/ValueObject";

export class Longitude extends ValueObject<number> {
    private constructor(value: number) {
        super(value);
    }

    public static new(value: number): Longitude {
        return new Longitude(value);
    }

    public static fromString(value: string): Longitude {
        const parsed = Number.parseFloat(value);

        if (isNaN(parsed)) {
            throw new Error('Not a number');
        }

        return new Longitude(parsed);
    }
}
