import { Address } from "../domain/Address";
import { GeocodedAddress } from "./GeocodedAddress";
import { IGeocodingService } from "./IGeocoding.service";
import https from "https";
import http from "http";
import { Transform } from "stream";
import { NominatimGeocodedAddress } from "./NominatimResponse";
import { Latitude } from "./Latitude";
import { Longitude } from "./Longitude";
import { GeocodeSearchResult } from "./GeocodeSearchResult";

export class NominatimGeocodingService implements IGeocodingService {
    private static readonly API_URI = 'https://nominatim.openstreetmap.org/search?q=**QUERY_STRING**&format=json'

    private getSearchUrl(address: Address): URL {
        const uri = encodeURI(address.value);
        const rul = NominatimGeocodingService.API_URI.replace('**QUERY_STRING**', uri);

        return new URL(rul);
    }

    private callNominatimSearchApi(url: URL): Promise<NominatimGeocodedAddress[]> {
        return new Promise((resolve, reject) => {
            const protocol = url.protocol == 'http:' ? http : https;
            const request = protocol.get({
                host: url.hostname,
                path: url.pathname + url.search,
                port: url.port,
                headers: {
                    'Accept': 'application/json',
                    'User-Agent': 'geocoding-recruitment-task'
                }
            }, res => {
                if (res.statusCode !== 200) {
                    reject(new Error(res.statusMessage));
                }

                const buffer = new Transform();

                res.on('data', chunk => buffer.push(chunk))
                    .on('error', err => {
                        reject(err);
                    }).on('end', () => {
                        const data = JSON.parse(buffer.read().toString());
                        resolve(data);
                    });
            });

            request.on('error', err => {
                reject(err);
            })
        });
    }

    async geocode(address: Address): Promise<GeocodeSearchResult> {
        const url = this.getSearchUrl(address);
        const geocodedAddresses = await this.callNominatimSearchApi(url);
        const geocoded = geocodedAddresses.map(gc => {
            return new GeocodedAddress(Address.new(gc.display_name), Latitude.new(+gc.lat), Longitude.new(+gc.lon))
        })

        return new GeocodeSearchResult(address, geocoded);
    }
}
