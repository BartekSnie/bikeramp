import { Address } from "../domain/Address";
import { GeocodedAddress } from "./GeocodedAddress";

export class GeocodeSearchResult {
    search: Address;
    geocoded: GeocodedAddress[];

    constructor(searchBy: Address, geocoded: GeocodedAddress[] = []) {
        this.search = searchBy;
        this.geocoded = geocoded;
    }

    public getResult(index: number): GeocodedAddress | null {
        return this.geocoded[index] ?? null;
    }

    public getFirstResult(): GeocodedAddress | null {
        return this.geocoded[0] ?? null;
    }
}
