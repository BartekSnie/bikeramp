export interface NominatimGeocodedAddress {
    lat: string;
    lon: string;
    display_name: string;
}
