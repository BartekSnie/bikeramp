import { Address } from "../domain/Address";
import { GeocodedAddress } from "./GeocodedAddress";
import { GeocodeSearchResult } from "./GeocodeSearchResult";

export interface IGeocodingService {
    geocode(address: Address): Promise<GeocodeSearchResult>;
}
