import { Address } from "../domain/Address";
import { Latitude } from "./Latitude";
import { Longitude } from "./Longitude";

export class GeocodedAddress {
    address: Address;
    latitude: Latitude;
    longitude: Longitude;

    public constructor(address: Address, latitude: Latitude, longitude: Longitude) {
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }
}