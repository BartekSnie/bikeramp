import { Latitude } from "../geocoding/Latitude";
import { Longitude } from "../geocoding/Longitude";

export interface IDistanceService {
    calculate(lat1: Latitude, lan1: Longitude, lat2: Latitude, lan2: Longitude): number
}
