import { Latitude } from "../geocoding/Latitude";
import { Longitude } from "../geocoding/Longitude";
import { IDistanceService } from "./IDistance.service";

export class HaversineDistance implements IDistanceService {
    public static EARTH_RADIUS_IN_KM = 6378.8;
    private static DEGREE_TO_RADIAN_RATIO = 0.01745329251;

    private convertDegreeValueToRadian(value: number): number {
        return value * HaversineDistance.DEGREE_TO_RADIAN_RATIO
    }

    calculate(lat1: Latitude, lon1: Longitude, lat2: Latitude, lon2: Longitude): number {
        const latDelta = this.convertDegreeValueToRadian(lat2.value - lat1.value);
        const lonDelta = this.convertDegreeValueToRadian(lon2.value - lon1.value);
        const rLat1 = this.convertDegreeValueToRadian(lat1.value);
        const rLat2 = this.convertDegreeValueToRadian(lat2.value);

        const base =
            Math.pow(Math.sin(latDelta * 0.5), 2) +
            (
                Math.cos(rLat2) *
                Math.cos(rLat1) *
                Math.pow(Math.sin(lonDelta * 0.5), 2)
            );
        return 2 * HaversineDistance.EARTH_RADIUS_IN_KM * Math.asin(Math.sqrt(base));
    }
}