import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HaversineDistance } from 'src/core/distance/HaversineDistance.service';
import { NominatimGeocodingService } from 'src/core/geocoding/NominatimGeocodingService';
import { TripRepository } from 'src/core/trips/Trip.repository';
import { TripService } from 'src/core/trips/Trip.service';
import { TripEntity } from 'src/db/entities/Trip.entity';
import { TripsController } from './trips.controller';

@Module({
  imports: [TypeOrmModule.forFeature([TripEntity])],
  controllers: [TripsController],
  providers: [
    {
      provide: 'GeocodingService',
      useClass: NominatimGeocodingService
    },
    {
      provide: 'DistanceService',
      useClass: HaversineDistance
    },
    {
      provide: 'TripService',
      useClass: TripService
    },
    {
      provide: 'TripRepository',
      useClass: TripRepository
    }
  ]
})
export class TripsModule { }
