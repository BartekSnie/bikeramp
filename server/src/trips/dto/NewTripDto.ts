import { IsDateString, IsNotEmpty, IsNumber, IsString } from "class-validator";

export class NewTripDto {
    @IsString()
    @IsNotEmpty()
    start_address: string;

    @IsString()
    @IsNotEmpty()
    destination_address: string;

    @IsNumber({
        allowInfinity: false,
        allowNaN: false,
        maxDecimalPlaces: 0
    })
    price: number;

    @IsDateString()
    date: string;
}
