import { Body, Controller, HttpException, HttpStatus, Inject, Post } from '@nestjs/common';
import { Address } from 'src/core/domain/Address';
import { Price } from 'src/core/domain/Price';
import { ITripService } from 'src/core/trips/ITrip.service';
import { NewTripDto } from './dto/NewTripDto';

@Controller('trips')
export class TripsController {
    constructor(
        @Inject('TripService')
        private readonly tripService: ITripService
    ) { }

    @Post()
    async registerNewTrip(
        @Body() body: NewTripDto
    ): Promise<void> {
        try {
            await this.tripService.registerNew({
                startingAddress: Address.new(body.start_address),
                destinationAddress: Address.new(body.destination_address),
                date: body.date,
                price: Price.new(body.price)
            });
        } catch (error) {
            throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
        }
    }
}
